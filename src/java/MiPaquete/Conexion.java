/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MiPaquete;
import java.sql.*; 
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gileto
 */
public class Conexion 
{
    Connection conexion;
    ResultSet resultado;
    Statement sencencia;
    String url="jdbc:mysql://localhost/seguridad";
    String usuario="beto";
    String pass="beto";
    String acceso;
    public Conexion()
    {
    conexion=null;
    resultado=null;
    sencencia=null; 
    }
    public boolean crearConexion() throws SQLException
    {
        boolean b=false;
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            conexion=(Connection)DriverManager.getConnection(url,usuario,pass);
            System.out.println(conexion);
            sencencia=conexion.createStatement();
            
        }
        catch (ClassNotFoundException ex) 
        {
            b=true;
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return b;
    }
  public boolean verificar(String nombre,String id) 
    {
        boolean b=false;
       try {
           String sql="select * from login where nombre=? and pass=? ;";           
           PreparedStatement sentecnia=conexion.prepareStatement(sql);
           sentecnia.setString(1, nombre);
           sentecnia.setString(2, id);
           ResultSet resultado= null;
           System.out.println("Ejecutar SQL: " + sql);
           if (sentecnia.execute()) {
               resultado = sentecnia.getResultSet();
               if (resultado.next()) {
                   b = true;
               }
               // Existe al menos un registro
           }          
           
       } catch (SQLException ex) {
           Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
       }
       return b;
    }
    
}
